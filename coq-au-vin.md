# Coq Au Vin

> Serves 4

## Ingredients

- 150g streaky bacon
- 4 skinless chicken breasts
- 225g shallots
- 225g baby button mushrooms
- 300ml chicken stock
- 300ml red wine
- 2 bay leaves
- half tube tomato puree
- 2 tablespoons plain white flour
- salt and pepper (to taste)

## Equipment

- Wok
- Measuring jug

## Method

1. Prepare the bacon, mushrooms and shallots. Cut the bacon into strips approximately 1cm in width. Wash the mushrooms in cold water. Cut the ends off the shallots, peel them and then cut into smaller pieces.

2. Dry fry the bacon in a wok on a medium heat for approximately 5 minutes until the bacon turns crispy.

  > Keep turning the bacon from the first moment to stop it from sticking to the pan.

  > Turn the heat down to stop the bacon from burning if necessary.

3. Add the chicken and shallots to the pan and cook for 5 minutes. After approximately 1 minute add the mushrooms. Continue cooking until the chicken is sealed on all sides.

  > When adding the chicken, push the bacon to the side of the wok. After adding the chicken scoop the bacon back over the top of the chicken.

4. Add the chicken stock, red wine, tomato puree, bay leaves and seasoning.

  > Measure the red wine in the measuring jug and add first, then measure and add the chicken stock. This avoids having to put the red wine in a hot measuring jug.

  > Be gentle with the bay leaves at first to avoid them breaking up.

5. Stir well and bring to the boil.

6. Simmer on a low heat for approximately 1.5 hours with the top of the wok on. Stir well approximately every 30 minutes.

7. Remove the bay leaves.

8. Turn up the heat to bring it to the boil.

9. Mix the plain white flour with water to make a paste with the consistency of cream. Add and stir it in to thicken the sauce.

 > Make sure the four and water mixture is smooth, with no lumps.

10. Serve by putting one chicken breast and some of the sauce/other ingredients on each place.

## Reheating

Coq au vin can be frozen and reheated.

1. Cook at 180c in an oven for approximately 30 minutes or until it's bubbling.

## Tips

- Serve with mash potato, red wine and crusty bread and butter.
